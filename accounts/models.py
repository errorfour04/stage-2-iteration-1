from django.db import models
from django.contrib.auth.models import User
from modules.models import Module
from django.db.models.signals import post_save
from django.dispatch import receiver
from constants.types import ACCOUNT_TYPES


class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    account_type = models.CharField(
        max_length=50, choices=ACCOUNT_TYPES, default='STUDENT')
    modules = models.ManyToManyField(Module, blank=True)


# Ensure concurrency between accounts
@receiver(post_save, sender=User)
def create_user_account(sender, instance, created, **kwargs):
    if created:
        Account.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_account(sender, instance, **kwargs):
    instance.account.save()
