from django.db import models
from datetime import datetime
from profiles.models import Profile


class Module(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=datetime.now)

    # Calculate time spent on a module
    @property
    def time_spent(self):
        from assignments.models import Assignment

        time = 0
        for assignment in Assignment.objects.filter(module=self):
            time += assignment.time_spent

        return time

    def __str__(self):
        return self.name
