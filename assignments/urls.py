from django.urls import path
from . import views

urlpatterns = [
    path('<int:assignment_id>', views.assignment, name='assignment'),
    path('<int:assignment_id>/create_task',
         views.create_task, name='create_task'),
    path('<int:assignment_id>/do_activity', views.do_activity, name='do_activity'),
    path('<int:assignment_id>/add_activity', views.add_activity, name='add_activity'),
    path('<int:assignment_id>/add_milestone', views.add_milestone, name='add_milestone'),
    path('viewm/<mil_name>', views.view_milestone, name='view_milestone')
]
