from django.contrib import admin
from .models import Account


class AccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'account_type')
    list_display_links = ('id',)
    list_filter = ('account_type',)

    list_per_page = 25


admin.site.register(Account, AccountAdmin)
